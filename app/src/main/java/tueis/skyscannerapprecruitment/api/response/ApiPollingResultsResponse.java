package tueis.skyscannerapprecruitment.api.response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import tueis.skyscannerapprecruitment.SkyscannerActivity;
import tueis.skyscannerapprecruitment.SkyscannerApp;
import tueis.skyscannerapprecruitment.models.Carrier;
import tueis.skyscannerapprecruitment.models.Itinerary;
import tueis.skyscannerapprecruitment.models.Leg;

/**
 * ApiPollingResultsResponse treats the received JSON from Skyscanner's API with the polling results (flights information)
 * Created by Oriol on 06/11/2017.
 */

public class ApiPollingResultsResponse {
    private SkyscannerActivity mActivity;

    public ApiPollingResultsResponse(SkyscannerActivity activity) {
        this.mActivity = activity;
    }

    /**
     * Treats the response from server and saves all the needed information (itineraries, legs, carriers and places)
     *
     * @param jsonObject response from server
     */
    public void treatJSONResponse(JSONObject jsonObject) {
        try {
            JSONArray itinerariesArray = jsonObject.getJSONArray("Itineraries");
            Itinerary itinerary;
            JSONArray pricingOptionsArray, agentsArray;
            JSONObject jsonItinerary, jsonPricingOptions;
            ArrayList<Itinerary> itineraries = new ArrayList<>();
            int i;
            for (i = 0; i < itinerariesArray.length(); i++) {
                jsonItinerary = new JSONObject(String.valueOf(itinerariesArray.get(i)));
                itinerary = new Itinerary();
                itinerary.setOutboundLegId(jsonItinerary.getString("OutboundLegId"));
                itinerary.setInboundLegId(jsonItinerary.getString("InboundLegId"));
                pricingOptionsArray = jsonItinerary.getJSONArray("PricingOptions");
                if (pricingOptionsArray.length() > 0) {
                    jsonPricingOptions = new JSONObject(String.valueOf(pricingOptionsArray.get(0)));
                    agentsArray = jsonPricingOptions.getJSONArray("Agents");
                    if (agentsArray.length() > 0) itinerary.setAgent(agentsArray.get(0).toString());
                    itinerary.setPrice(jsonPricingOptions.getDouble("Price"));
                }
                itineraries.add(itinerary);
            }
            SkyscannerApp.setHowManyItineraries(i);
            SkyscannerApp.setItineraries(itineraries);

            JSONArray legsArray = jsonObject.getJSONArray("Legs");
            JSONArray carriersArray, segmentIdsArray;
            Leg leg;
            HashMap<String, Leg> legs = new HashMap<>();
            JSONObject jsonLeg;
            for (int z = 0; z < legsArray.length(); z++) {
                jsonLeg = new JSONObject(String.valueOf(legsArray.get(z)));
                leg = new Leg();
                leg.setId(jsonLeg.getString("Id"));
                leg.setDeparture(jsonLeg.getString("Departure"));
                leg.setArrival(jsonLeg.getString("Arrival"));
                leg.setDuration(jsonLeg.getInt("Duration"));
                leg.setOriginStation(jsonLeg.getString("OriginStation"));
                leg.setDestinationStation(jsonLeg.getString("DestinationStation"));
                carriersArray = jsonLeg.getJSONArray("Carriers");
                if (carriersArray.length() > 0) leg.setCarrier(carriersArray.get(0).toString());
                segmentIdsArray = jsonLeg.getJSONArray("SegmentIds");
                leg.setStops(segmentIdsArray.length() - 1);
                legs.put(leg.getId(), leg);
            }
            SkyscannerApp.setLegs(legs);

            JSONArray carriersJSONArray = jsonObject.getJSONArray("Carriers");
            Carrier carrier;
            HashMap<String, Carrier> carriers = new HashMap<>();
            JSONObject jsonCarrier;
            for (int j = 0; j < carriersJSONArray.length(); j++) {
                jsonCarrier = new JSONObject(String.valueOf(carriersJSONArray.get(j)));
                carrier = new Carrier();
                carrier.setId(jsonCarrier.getString("Id"));
                carrier.setCode(jsonCarrier.getString("Code"));
                carrier.setName(jsonCarrier.getString("Name"));
                carrier.setImageUrl(jsonCarrier.getString("ImageUrl"));
                carriers.put(carrier.getId(), carrier);
            }
            SkyscannerApp.setCarriers(carriers);

            JSONArray placesJSONArray = jsonObject.getJSONArray("Places");
            HashMap<String, String> places = new HashMap<>();
            JSONObject jsonPlace;
            for (int j = 0; j < placesJSONArray.length(); j++) {
                jsonPlace = new JSONObject(String.valueOf(placesJSONArray.get(j)));
                places.put(jsonPlace.getString("Id"), jsonPlace.getString("Code"));
            }
            SkyscannerApp.setPlaces(places);

        } catch (Exception e) {
            e.printStackTrace();
            mActivity.hideLoadingDialog();
        }
    }
}

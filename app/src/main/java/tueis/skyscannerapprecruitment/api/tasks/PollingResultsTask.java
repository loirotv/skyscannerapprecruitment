package tueis.skyscannerapprecruitment.api.tasks;

import android.os.AsyncTask;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import tueis.skyscannerapprecruitment.SkyscannerActivity;
import tueis.skyscannerapprecruitment.R;
import tueis.skyscannerapprecruitment.SkyscannerApp;
import tueis.skyscannerapprecruitment.api.response.ApiPollingResultsResponse;

/**
 * Sends the session Key to Skyscanners's API and receive the desired information about flights.
 * Created by Oriol on 05/11/2017.
 */

public class PollingResultsTask extends AsyncTask<String, Void, String> {

    private static SkyscannerActivity mActivity;
    private String sessionKey;

    protected PollingResultsTask(SkyscannerActivity activity, String sessionKey) {
        this.mActivity = activity;
        this.sessionKey = sessionKey;
    }

    protected void onPreExecute() {
    }

    protected String doInBackground(String... params) {
        try {
            URL obj = new URL(SkyscannerApp.buildUrl("pricing/v1.0/" + sessionKey + "?apiKey=" + mActivity.getResources().getString(R.string.api_key)));
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("GET");
            conn.connect();

            int status = conn.getResponseCode();
            System.out.println("Status:" + "\t" + status);

            if (status == 200) {
                String stringResponse = readFullyAsString(conn.getInputStream(), "UTF-8");
                System.out.println(stringResponse);
                ApiPollingResultsResponse apiPollingResultsResponse = new ApiPollingResultsResponse(mActivity);
                apiPollingResultsResponse.treatJSONResponse(new JSONObject(stringResponse));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    protected void onPostExecute(String result) {
        mActivity.showResults();
    }

    /**
     * Helper method to transform InputStream to String
     *
     * @param inputStream
     * @param encoding
     * @return String parsed from inputStream
     * @throws IOException
     */
    private String readFullyAsString(InputStream inputStream, String encoding) throws IOException {
        return readFully(inputStream).toString(encoding);
    }

    /**
     * Helper method to transform InputStream to ByteArrayOutputStream
     *
     * @param inputStream
     * @return ByteArrayOutputStream parsed from inputStream
     * @throws IOException
     */
    private ByteArrayOutputStream readFully(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, length);
        }
        return byteArrayOutputStream;
    }

}



package tueis.skyscannerapprecruitment.api.tasks;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tueis.skyscannerapprecruitment.SkyscannerActivity;
import tueis.skyscannerapprecruitment.R;
import tueis.skyscannerapprecruitment.SkyscannerApp;

/**
 * Creates session with Skyscanner's API. Sends search information (hardcoded) and receive a session key.
 * Created by Oriol on 05/11/2017.
 */

public class CreateSessionTask extends AsyncTask<String, Void, String> {

    private SkyscannerActivity mActivity;

    private String sessionKey;
    private HashMap<String, String> paramsHM;

    public CreateSessionTask(SkyscannerActivity activity, HashMap<String, String> params) {
        this.mActivity = activity;
        this.paramsHM = params;
    }

    protected void onPreExecute() {
    }

    protected String doInBackground(String... params) {
        String location = null;
        try {
            URL obj = new URL(SkyscannerApp.buildUrl("pricing/v1.0"));
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");

            String urlParameters = "apiKey=" + mActivity.getResources().getString(R.string.api_key) + "&country=UK&currency=GBP&locale=en-UK" +
                    "&originplace=EDI-sky&destinationplace=LOND-sky" +
                    "&outbounddate=" + paramsHM.get("outboundDate") + "&inbounddate=" + paramsHM.get("inboundDate") +
                    "&locationschema=iata&cabinclass=Economy&adults=1&children=0&infants=0&groupPricing=false";
            DataOutputStream dStream = new DataOutputStream(conn.getOutputStream());
            dStream.writeBytes(urlParameters);
            dStream.flush();
            dStream.close();
            conn.connect();

            int status = conn.getResponseCode();
            System.out.println("Status:" + "\t" + status);

            if (status == 201) {
                Map<String, List<String>> map = conn.getHeaderFields();

                location = conn.getHeaderField("Location");
                if (location == null) {
                    System.out.println("Key 'Location' is not found!");
                } else {
                    System.out.println("Location - " + location);
                    sessionKey = location.substring(location.lastIndexOf("/") + 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    protected void onPostExecute(String result) {
        if (sessionKey != null) {
            Log.i("Session Created", sessionKey);
            new PollingResultsTask(mActivity, sessionKey).execute();
        } else {
            Toast.makeText(mActivity.getBaseContext(), mActivity.getResources().getString(R.string.base_error),
                    Toast.LENGTH_SHORT).show();
            mActivity.hideLoadingDialog();
        }
    }
}



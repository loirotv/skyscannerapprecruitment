package tueis.skyscannerapprecruitment;

import android.app.Application;
import android.os.StrictMode;

import java.util.ArrayList;
import java.util.HashMap;

import tueis.skyscannerapprecruitment.models.Carrier;
import tueis.skyscannerapprecruitment.models.Itinerary;
import tueis.skyscannerapprecruitment.models.Leg;

/**
 * Created by Oriol on 04/11/2017.
 */

public class SkyscannerApp extends Application{

    private static SkyscannerApp sInstance;
    public static final String BASE_URL = "http://partners.api.skyscanner.net/apiservices/";
    private static ArrayList<Itinerary> itineraries;
    private static HashMap<String, Leg> legs;
    private static HashMap<String, Carrier> carriers;
    private static HashMap<String, String> places;
    private static int howManyItineraries;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        itineraries = new ArrayList<>();
        legs = new HashMap<>();
        carriers = new HashMap<>();
        places = new HashMap<>();
        howManyItineraries = 0;
    }

    public static String buildUrl(String path) {
        return BASE_URL + path;
    }

    public static synchronized SkyscannerApp getsInstance() {
        return sInstance;
    }

    public static ArrayList<Itinerary> getItineraries() {
        return itineraries;
    }

    public static void setItineraries(ArrayList<Itinerary> itineraries) {
        SkyscannerApp.itineraries = itineraries;
    }

    public static HashMap<String, Leg> getLegs() {
        return legs;
    }

    public static void setLegs(HashMap<String, Leg> legs) {
        SkyscannerApp.legs = legs;
    }

    public static HashMap<String, Carrier> getCarriers() {
        return carriers;
    }

    public static void setCarriers(HashMap<String, Carrier> carriers) {
        SkyscannerApp.carriers = carriers;
    }

    public static HashMap<String, String> getPlaces() {
        return places;
    }

    public static void setPlaces(HashMap<String, String> places) {
        SkyscannerApp.places = places;
    }

    public static int getHowManyItineraries() {
        return howManyItineraries;
    }

    public static void setHowManyItineraries(int howManyItineraries) {
        SkyscannerApp.howManyItineraries = howManyItineraries;
    }
}

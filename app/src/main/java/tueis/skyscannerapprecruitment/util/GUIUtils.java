package tueis.skyscannerapprecruitment.util;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import tueis.skyscannerapprecruitment.R;

/**
 * Util that helps showing a loading dialog
 * Created by Oriol on 04/11/2017.
 */

public class GUIUtils {

    public static Dialog showLoadingDialog(final Context context) {

        final Dialog dialog = new Dialog(context);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.loading_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(false);

        return dialog;
    }
}

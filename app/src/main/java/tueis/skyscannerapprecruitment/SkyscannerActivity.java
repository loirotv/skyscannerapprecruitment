package tueis.skyscannerapprecruitment;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import tueis.skyscannerapprecruitment.api.tasks.CreateSessionTask;
import tueis.skyscannerapprecruitment.controllers.adapters.OffersAdapter;
import tueis.skyscannerapprecruitment.util.GUIUtils;

public class SkyscannerActivity extends AppCompatActivity {

    private TextView cities, howManyResults, dates;
    private RecyclerView offers;
    private Dialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadingDialog = GUIUtils.showLoadingDialog(this);

        cities = (TextView) findViewById(R.id.cities);

        // Set hardcoded cities text
        cities.setText(getResources().getString(R.string.cities));
        offers = (RecyclerView) findViewById(R.id.offers);
        howManyResults = (TextView) findViewById(R.id.how_many_results);
        dates = (TextView) findViewById(R.id.dates);
        createSessionAPI();
    }

    /**
     * Show the results received from Skyscanner's API
     */
    public void showResults() {
        try {
            int size = SkyscannerApp.getHowManyItineraries();
            String howMany = size + " of " + size + " results shown";
            howManyResults.setText(howMany);
            setAdapter(SkyscannerApp.getItineraries());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Attaches OffersAdapter to offers recycler view with the results received from server.
     *
     * @param items
     */
    private void setAdapter(ArrayList items) {
        if (items != null && items.size() > 0) {
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            offers.setLayoutManager(llm);
            OffersAdapter offersListAdapter = new OffersAdapter(this, items, this);
            offersListAdapter.notifyDataSetChanged();
            offers.setAdapter(offersListAdapter);
            hideLoadingDialog();
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_results),
                    Toast.LENGTH_SHORT).show();
            hideLoadingDialog();
        }
    }

    /**
     * Starts the call from the API to create a session
     */
    private void createSessionAPI() {
        showLoadingDialog();

        HashMap<String, String> params = new HashMap<>();
        Calendar date = Calendar.getInstance();
        // Finds date of next Monday
        while (date.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            date.add(Calendar.DATE, 1);
        }
        Locale locale = new Locale("en");
        SimpleDateFormat formatSDF = new SimpleDateFormat("yyyy-MM-dd", locale);
        params.put("outboundDate", formatSDF.format(date.getTime()));
        String datesText = new SimpleDateFormat("MMM dd., EEE - ", locale).format(date.getTime());
        date.add(Calendar.DATE, 1);
        datesText = datesText.concat(new SimpleDateFormat("MMM dd., EEE", locale).format(date.getTime()));
        dates.setText(datesText);
        params.put("inboundDate", formatSDF.format(date.getTime()));
        new CreateSessionTask(this, params).execute();
    }

    /**
     * Show Loading Dialog Box
     */
    public void showLoadingDialog() {
        Activity context = this;
        if ((context != null) && !context.isFinishing() && (loadingDialog != null)) {
            try {
                loadingDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Hide Loading Dialog Box
     */
    public void hideLoadingDialog() {
        Activity context = this;
        if (context != null && !context.isFinishing() && loadingDialog != null) {
            try {
                loadingDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

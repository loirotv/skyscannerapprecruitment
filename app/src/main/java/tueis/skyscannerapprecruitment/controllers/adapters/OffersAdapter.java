package tueis.skyscannerapprecruitment.controllers.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tueis.skyscannerapprecruitment.SkyscannerActivity;
import tueis.skyscannerapprecruitment.R;
import tueis.skyscannerapprecruitment.SkyscannerApp;
import tueis.skyscannerapprecruitment.models.Carrier;
import tueis.skyscannerapprecruitment.models.Itinerary;
import tueis.skyscannerapprecruitment.models.Leg;

/**
 * Adapter for the offers recycler view.
 * Created by Oriol on 04/11/2017.
 */

public class OffersAdapter extends RecyclerView.Adapter {

    private SkyscannerActivity mActivity;
    private ArrayList data = new ArrayList();
    private Context mContext;

    public OffersAdapter(Context mContext, ArrayList data, SkyscannerActivity mActivity) {
        this.mActivity = mActivity;
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.offer_item, parent, false);
        return new OfferHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder.getClass() == OfferHolder.class) {
            final OfferHolder oh = (OfferHolder) holder;
            final Itinerary itinerary = (Itinerary) data.get(position);
            String currency = "£";
            String price = currency + itinerary.getPrice();
            oh.price.setText(price);
            Leg leg = SkyscannerApp.getLegs().get(itinerary.getOutboundLegId());

            String duration = leg.getDuration() / 60 + "h " + leg.getDuration() % 60 + "m";
            oh.outbound_duration.setText(duration);
            String outboundTime = leg.getDeparture().substring(11, 16) + " - " + leg.getArrival().substring(11, 16);
            oh.outbound_time.setText(outboundTime);
            if (leg.getStops() == 0) {
                oh.outbound_direct.setText(mActivity.getResources().getText(R.string.direct));
            } else {
                String stopsString = leg.getStops() + " " + mActivity.getResources().getText(R.string.stops);
                oh.outbound_direct.setText(stopsString);
            }

            Carrier carrier = SkyscannerApp.getCarriers().get(leg.getCarrier());

            String flightAirportsAirlines = SkyscannerApp.getPlaces().get(leg.getOriginStation())
                    + " - " +
                    SkyscannerApp.getPlaces().get(leg.getDestinationStation())
                    + ", " + carrier.getName();
            oh.outbound_airports_and_airlines.setText(flightAirportsAirlines);
            Picasso.with(mActivity)
                    .load(carrier.getImageUrl())
                    .fit()
                    .error(R.drawable.favicon)
                    .into(oh.outbound_favicon, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });

            leg = SkyscannerApp.getLegs().get(itinerary.getInboundLegId());
            duration = leg.getDuration() / 60 + "h " + leg.getDuration() % 60 + "m";
            oh.return_duration.setText(duration);
            String returnTime = leg.getDeparture().substring(11, 16) + " - " + leg.getArrival().substring(11, 16);
            oh.return_time.setText(returnTime);
            if (leg.getStops() == 0) {
                oh.return_direct.setText(mActivity.getResources().getText(R.string.direct));
            } else {
                String stopsString = leg.getStops() + " " + mActivity.getResources().getText(R.string.stops);
                oh.return_direct.setText(stopsString);
            }
            carrier = SkyscannerApp.getCarriers().get(leg.getCarrier());
            flightAirportsAirlines = SkyscannerApp.getPlaces().get(leg.getOriginStation())
                    + " - " +
                    SkyscannerApp.getPlaces().get(leg.getDestinationStation())
                    + ", " + carrier.getName();
            oh.return_airports_and_airlines.setText(flightAirportsAirlines);
            Picasso.with(mActivity)
                    .load(carrier.getImageUrl())
                    .fit()
                    .error(R.drawable.favicon)
                    .into(oh.return_favicon, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });


            // Fake satisfaction
            double random = Math.random() * 10;
            oh.score.setText(String.format("%.1f", random));
            if (random > 7.5) {
                oh.satisfaction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_sentiment_very_satisfied_black_24px));
            } else if (random > 5) {
                oh.satisfaction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_sentiment_satisfied_black_24px));
            } else if (random > 2.5) {
                oh.satisfaction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_sentiment_dissatisfied_black_24px));
            } else {
                oh.satisfaction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_sentiment_very_dissatisfied_black_24px));
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    /**
     * Holder for each of the offers (itinerary) shown in the recycler view.
     */
    public static class OfferHolder extends RecyclerView.ViewHolder {

        private RelativeLayout outbound, return_flight;
        private ImageView outbound_favicon, return_favicon, satisfaction;
        private TextView outbound_time, return_time, outbound_airports_and_airlines,
                return_airports_and_airlines, outbound_direct, return_direct,
                outbound_duration, return_duration, score, cheapest, shortest, price, bookings_required;


        public OfferHolder(View itemView) {
            super(itemView);
            outbound = (RelativeLayout) itemView.findViewById(R.id.outbound);
            return_flight = (RelativeLayout) itemView.findViewById(R.id.return_flight);

            outbound_favicon = (ImageView) itemView.findViewById(R.id.outbound_favicon);
            return_favicon = (ImageView) itemView.findViewById(R.id.return_favicon);
            satisfaction = (ImageView) itemView.findViewById(R.id.satisfaction);

            outbound_time = (TextView) itemView.findViewById(R.id.outbound_time);
            return_time = (TextView) itemView.findViewById(R.id.return_time);
            outbound_airports_and_airlines = (TextView) itemView.findViewById(R.id.outbound_airports_and_airline);
            return_airports_and_airlines = (TextView) itemView.findViewById(R.id.return_airports_and_airline);
            outbound_direct = (TextView) itemView.findViewById(R.id.outbound_direct);
            return_direct = (TextView) itemView.findViewById(R.id.return_direct);
            outbound_duration = (TextView) itemView.findViewById(R.id.outbound_duration);
            return_duration = (TextView) itemView.findViewById(R.id.return_duration);
            score = (TextView) itemView.findViewById(R.id.score);
            cheapest = (TextView) itemView.findViewById(R.id.cheapest);
            shortest = (TextView) itemView.findViewById(R.id.shortest);
            price = (TextView) itemView.findViewById(R.id.price);
            bookings_required = (TextView) itemView.findViewById(R.id.bookings_required);
        }
    }
}
